class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
      return this._name;
    }
  
    get age() {
      return this._age;
    }
  
    get salary() {
      return this._salary;
    }
  
    set name(name) {
      this._name = name;
    }
  
    set age(age) {
      this._age = age;
    }
  
    set salary(salary) {
      this._salary = salary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    get salary() {
      return this._salary * 3;
    }
    get lang() {
      return this._lang;
    }
  
    set lang(newLang) {
      this._lang = newLang;
    }
  }
  
  const employee = new Employee("Iryna", "34", 5000, ["Ukrainian"]);
  
  const programmerOne = new Programmer("Petro", "20", 4000, ["English", "Spanish"]);
  const programmerTwo = new Programmer("Vasyl", "45", 6000, ["Ukrainian"]);
  const programmerThree = new Programmer("Nataly", "30", 5500, ["German"]);
  
  console.log(employee.salary);
  console.log(programmerOne);
  console.log(programmerTwo);
  console.log(programmerThree);
  